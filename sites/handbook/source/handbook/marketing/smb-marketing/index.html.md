---
layout: handbook-page-toc
title: "SMB Marketing"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Who we are
SMB Marketing addresses the [SMB Segment](/handbook/sales/field-operations/gtm-resources/#segmentation) in support of [SMB Sales](/handbook/sales/commercial/#smb-account-executives), [Sales Development (SDR) in Marketing](/handbook/marketing/revenue-marketing/sdr/), [Growth in Product](/handbook/product/growth/), and other teams.

### SMB Marketing Team

| PMM | Campaigns | Digital | Content | TMM | Channel | PMO |
| ---- | --- | --- | --- | --- | --- | --- |
| [Brian Glanz](https://gitlab.com/brianglanz) | [Indre Kryzeviciene](https://gitlab.com/ikryzeviciene),<br>[Shari Rances](https://gitlab.com/srances) | [Zac Badgley](https://gitlab.com/zbadgley) | [Sharon Gaudin](https://gitlab.com/sgaudin) | [William Arias](https://gitlab.com/warias) | [Karen Pourshadi](https://gitlab.com/kpourshadi) | [Kimberly Bolton](https://gitlab.com/kbolton1) |

Our DRI is [Brian Glanz](https://gitlab.com/brianglanz).

Team members are DRIs for their own tasks and report into their home teams, per GitLab's [DRI culture](/handbook/people-group/directly-responsible-individuals/) and [functional reporting lines](/handbook/leadership/no-matrix-organization/).

## How we work

### Collaborate with us
<a href="https://gitlab.com/gitlab-com/marketing/smb-marketing/activity"><img style="padding-right: 10px; padding-bottom: 10px" src="/images/all-remote/gitlab-value-tanukis_collaberation.svg" alt="Collaboration" title="Collaboration" height="40"></a>[SMB Marketing's GitLab project](https://gitlab.com/gitlab-com/marketing/smb-marketing/activity) helps to manage [our own issues](https://gitlab.com/gitlab-com/marketing/smb-marketing/-/boards), while we work across many teams. We [dogfood](/handbook/values/#dogfooding) GitLab to collaborate on ideas, gather feedback, and manage projects, including our [use of the Handbook](/handbook/handbook-usage/#why-handbook-first).

### Slack

Say 👋 in the [SMB Marketing Slack channel](https://gitlab.slack.com/archives/C02U0386T5Y).
